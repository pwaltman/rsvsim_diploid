setGeneric("simulateSV",
           function( output=".", 
                     genome, chrs, male=TRUE,
                     haploid=TRUE, csomeGroups=list(), maxNumCsomes=2, anneuploidyRate=0.25,
                     dels=0, itxs=0, ctxs=0, invs=0, dups=0, trans=0, 
                     size, sizeDels=10, sizeItxs=10, sizeCtxs=10, sizeInvs=10, sizeDups=10, 
                     regionsDels, regionsItxs, regionsCtxs, regionsInvs, regionsDups, regionsTrans, 
                     percHomozygous=0.25, maxDups=10, 
                     percCopiedIns=0, percCopiedItxs=0, percCopiedCtxs=0, 
                     percInvertedIns=0, percInvertedItxs=0, percInvertedCtxs=0, 
                     percBalancedTrans=1, bpFlankSize=20, percSNPs=0, indelProb=0, maxIndelSize=10, 
                     repeatBias=FALSE, weightsMechanisms, weightsRepeats, repeatMaskerFile, bpSeqSize=100, 
                     #dels=0, ins=0, invs=0, dups=0, trans=0, 
                     #size, sizeDels=10, sizeIns=10, sizeInvs=10, sizeDups=10, 
                     #regionsDels, regionsIns, regionsInvs, regionsDups, regionsTrans, 
                     random=TRUE, seed, verbose=TRUE) {
             standardGeneric("simulateSV")
           })

setGeneric("estimateSVSizes",
           function(n, svSizes, minSize=NA, maxSize=NA, default="deletions", hist=TRUE){
             standardGeneric("estimateSVSizes")
           })

setGeneric("compareSV",
           function(querySVs, simSVs, tol=200){
             standardGeneric("compareSV")
           })
