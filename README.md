# RSVSim_diploid
A heavily modified version of the RSVSim Bioconductor package that allows for the
generation of diploid & aneuploid synthetic genomes, whereas the original RSVSim tool
only allowed for the generation of haploid synthetic genomes. In other words, all
translocations were considered to be homozygous. Given the exceptionally unrealistic
likelihood of such an event, this was written to allow users to generate a somewhat
more realistic synthetic genome.